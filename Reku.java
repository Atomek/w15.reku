package rekursja;

import java.util.Scanner;

public class Reku {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Podaj słowo");
        String word = scanner.next();
        System.out.println(makeStars(word));
    }
    private static String  makeStars(String word) {
        if (word.length()==1){
            return word ;
        }
        return word.substring(0,1)+"*"+makeStars(word.substring(1));
    }
}
